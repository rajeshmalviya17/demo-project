import React from "react";
import Form, {
  ButtonItem,
  GroupItem,
  SimpleItem,
  Label,
  CompareRule,
  EmailRule,
  PatternRule,
  RangeRule,
  RequiredRule,
  StringLengthRule,
  AsyncRule,
  ButtonOptions,
} from "devextreme-react/form";
import { useTranslation } from "react-i18next";
import notify from 'devextreme/ui/notify';
const passwordOptions = {
  mode: "password",
};
type LoginProps = {
  onSubmit: Function;
  goToResetPassword: Function;
  error: boolean;
};
const dateBoxOptions = { width: "100%" };

const customer = {
  email: "",
  password: "",
};
const sendEmailRequest = (value: any) => {
  const validEmail = "rjs@gmail.com";
  return new Promise((resolve) => {
    setTimeout(function () {
      resolve(value == validEmail);
    }, 1000);
  });
};
const sendPasswordRequest = (value: any) => {
  const validPassword = "Rajesh17#";
  return new Promise((resolve) => {
    setTimeout(function () {
      resolve(value == validPassword);
    }, 1000);
  });
};
const asyncEmailValidation = (params: any) => {
  return sendEmailRequest(params.value);
};
const asyncPasswordValidation = (params: any) => {
  return sendPasswordRequest(params.value);
};
const Login: React.FC<LoginProps> = (props: LoginProps) => {
  const { t } = useTranslation();
  const { onSubmit, goToResetPassword, error } = props;
  const [isSubmitted, setIsSubmitted] = React.useState(false);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    setIsSubmitted(!isSubmitted);
    onSubmit(customer);
    notify("Login ");
  };

  return (
    <form
      action="your-action"
      onSubmit={handleSubmit}
      className="flex flex-col justify-center "
    >
      <div className="bg-green-400 md:container md:mx-auto mx-auto w-80 px-7 rounded ">
        <div className="m-4">
          <h1>{t("auth.action.login")}</h1>
        </div>

        <Form
          formData={customer}
          readOnly={false}
          showColonAfterLabel={true}
          showValidationSummary={true}
          validationGroup="customerData"
        >
          <SimpleItem
            dataField={t("auth.label.email")}
            editorType="dxTextBox"
            editorOptions={dateBoxOptions}
          >
            <RequiredRule message={t("auth.validation.emailRequired")} />
            <EmailRule message={t("auth.validation.incorrectEmail")} />
            <AsyncRule
              message={t("auth.validation.registeredEmail")}
              validationCallback={asyncEmailValidation}
            />
          </SimpleItem>
          <SimpleItem
            dataField={t("auth.label.password")}
            editorType="dxTextBox"
            editorOptions={passwordOptions}
          >
            <RequiredRule message={t("auth.validation.passwordRequired")} />
            <AsyncRule
              message=""
              validationCallback={asyncPasswordValidation}
            />
          </SimpleItem>
          <ButtonItem>
            <ButtonOptions
              width={"100%"}
              type={"default"}
              useSubmitBehavior={true}
            >
              {t("adduser.label.submit")}
            </ButtonOptions>
          </ButtonItem>
        </Form>
      </div>
    </form>
  );
};

export default Login;
