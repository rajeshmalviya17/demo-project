const CracoLessPlugin = require('craco-less');


module.exports = {
  style: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
    babel: {
        presets: [],
        plugins: [],
        loaderOptions: {
          ignore: ['./node_modules/mapbox-gl/dist/mapbox-gl.js'],
        },
    },
};