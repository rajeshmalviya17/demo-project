import 'devextreme/dist/css/dx.light.css';
import { AppRoutes } from './AppRoutes';
import { CoreFacadeOperations, useCoreFacade } from './modules/core/facades/Core.facades';
function App() {
  const coreFacade: CoreFacadeOperations | null = useCoreFacade();
  coreFacade?.setUpApplication();
  return (
    <div className="bg-gradient-to-r from-purple-400 to-blue-500 h-full">
      <AppRoutes/>
    </div>
  );
}

export default App;
