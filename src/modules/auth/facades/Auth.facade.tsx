import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../core/models/interfaces/component.interface";
import { LoginVM } from "../models/class/VMs/Auth";
import {
  AuthServiceOperations,
  useAuthService,
} from "../services/Auth.service";

export interface AuthFacadeOperations {
  logIn: Function;
  isLoggedIn: Function;
  logOut:Function;
}

const AuthFacadeContext = createContext<AuthFacadeOperations | null>(null);

export const AuthFacadeProvider = (props: EAComponentProps) => {
  const authService: AuthServiceOperations | null = useAuthService();
  const logIn = (body: LoginVM) => {
    return authService?.logIn(body);
  };
  const isLoggedIn = () => {
    return authService?.isLoggedIn();
  };
  const logOut = () => {
    console.log("logout Facade");
    return authService?.logOut();
  };

  const AuthFacadeOperations: AuthFacadeOperations = {
    logIn,
    isLoggedIn,
    logOut
  };

  return (
    <AuthFacadeContext.Provider value={AuthFacadeOperations}>
      {props.children}
    </AuthFacadeContext.Provider>
  );
};

export const useAuthFacade = () => {
  return useContext(AuthFacadeContext);
};
