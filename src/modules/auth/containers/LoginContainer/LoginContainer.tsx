import * as React from "react";
import { AuthFacadeOperations, useAuthFacade } from "../../facades/Auth.facade";
import { LoginVM } from "../../models/class/VMs/Auth";
import Login from "../../presentations/Login/Login";

type LoginContainerProps = {
  onLoginSuccessful: Function;
};

const LoginContainer: React.FC<LoginContainerProps> = (
  props: LoginContainerProps
) => {
  const { onLoginSuccessful } = props;
  const authFacade: AuthFacadeOperations | null = useAuthFacade();
  const [error, setError] = React.useState(false);
  const handleOnSubmit = (data: LoginVM) => {
    setError(false);
    const response = authFacade?.logIn(data);

    if (response.status === 200) {
      onLoginSuccessful(response, data);
    } else {
      setError(true);
    }
  };
  return <Login onSubmit={handleOnSubmit} error={error}></Login>;
};

export default LoginContainer;
