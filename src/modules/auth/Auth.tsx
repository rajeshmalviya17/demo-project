import { useHistory } from "react-router";
import AuthRoutes from "./AuthRoutes";
import { AuthFacadeOperations, useAuthFacade } from "./facades/Auth.facade";
const Auth = () => {
  let history = useHistory();
  const authFacade: AuthFacadeOperations | null = useAuthFacade();
  if (authFacade?.isLoggedIn()) {
    history.push("/home");
  }

  return (
    <div>
      <AuthRoutes />
    </div>
  );
};

export default Auth;
