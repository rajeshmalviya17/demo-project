import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../core/models/interfaces/component.interface";
import { LoginVM } from "../models/class/VMs/Auth";
import {
  AuthRepoOperations,
  useAuthRepository,
} from "../repository/Auth.repository";

export interface AuthServiceOperations {
  logIn: Function;
  isLoggedIn: Function;
  logOut:Function;
}
const AuthServiceContext = createContext<AuthServiceOperations | null>(null);

export const AuthServiceProvider = (props: EAComponentProps) => {
  const authRepository: AuthRepoOperations | null = useAuthRepository();
  const logIn = (body: LoginVM) => {
    return authRepository?.logIn(body);
  };
  const isLoggedIn = () => {
    return authRepository?.isLoggedIn();
  };
  const logOut = () => {
    return authRepository?.logOut();
  };
  const AuthServiceOperations: AuthServiceOperations = {
    logIn,
    isLoggedIn,
    logOut
  };

  return (
    <AuthServiceContext.Provider value={AuthServiceOperations}>
      {props.children}
    </AuthServiceContext.Provider>
  );
};

export const useAuthService = () => {
  return useContext(AuthServiceContext);
};
