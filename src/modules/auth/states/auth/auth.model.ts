
export interface AuthState {
  email: string;
  isSignedIn: boolean;
}

export function createInitialAuthState(): AuthState {
  return {
    email: "",
    isSignedIn: false,
  };
}
