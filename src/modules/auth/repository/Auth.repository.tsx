import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../core/models/interfaces/component.interface";
import { ApiOperations, useAPIService } from "../../core/services/API.service";
import { LoginVM } from "../models/class/VMs/Auth";
import { authQuery, authStore } from "../states/auth";
export interface AuthRepoOperations {
  logIn: Function;
  isLoggedIn: Function;
  logOut:Function;
}
const AuthRepositoryContext = createContext<AuthRepoOperations | null>(null);

export const AuthRepositoryProvider = (props: EAComponentProps) => {
  const APIService: ApiOperations | null = useAPIService();
  const isLoggedIn = () => {
    return authQuery.getValue().isSignedIn;
  };
  const logIn = (body: LoginVM) => {
    return APIService?.login(body);
  };
  const logOut = () => {
    console.log("logout repository");
    
    return  authStore.update({ email: "", isSignedIn: false });
  };

  const AuthRepoOperations: AuthRepoOperations = {
    logIn,
    isLoggedIn,
    logOut
  };

  return (
    <AuthRepositoryContext.Provider value={AuthRepoOperations}>
      {props.children}
    </AuthRepositoryContext.Provider>
  );
};

export const useAuthRepository = () => {
  return useContext(AuthRepositoryContext);
};
