import * as React from "react";
import { useHistory } from "react-router-dom";
import { EAComponentProps } from "../../../core/models/interfaces/component.interface";
import LoginContainer from "../../containers/LoginContainer";
import { LoginDTO } from "../../models/class/DTOs/Auth";
import { LoginVM } from "../../models/class/VMs/Auth";
import { AUTH_CONST } from "../../models/enums/auth.enums";

const AuthPage: React.FC<EAComponentProps> = () => {
  let history = useHistory();

  if (window.performance) {
    if (performance.navigation.type === 1) {
      localStorage.removeItem(AUTH_CONST.TAB_CLOSED);
    } else {
      if (localStorage.getItem(AUTH_CONST.TAB_CLOSED) === null) {
        localStorage.setItem(AUTH_CONST.TAB_CLOSED, "yes");
      }
    }
  }

  const handleLoginSuccess = (response: LoginDTO, data: LoginVM) => {
    history.push("/home");
  };

  return <LoginContainer onLoginSuccessful={handleLoginSuccess} />;
};

export default AuthPage;
