import { Route, Switch, useRouteMatch } from "react-router";
import PageNotFound from "../core/presentations/PageNotFound";

import AuthPage from "./pages/AuthPage";

const AuthRoutes = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route path={match.path + "/"} exact={true}>
        <AuthPage />
      </Route>

      <Route>
        <PageNotFound />
      </Route>
    </Switch>
  );
};

export default AuthRoutes;
