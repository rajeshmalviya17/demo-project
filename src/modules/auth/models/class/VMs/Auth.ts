export class userVM {
    id: string = "";
    key: string = "";
    userImage: string = ""; //TODO image should be bynamic
    firstName: string = "";
    lastName: string = "";
    fullName: string = "";
    emailAddress: string = "";
    userType: Array<string> = [];
    phoneNumber: string = "";
    lastActivity: string = "";
  }
  export class CreatePasswordVM {
    "password" : string =  "";
    "confirmPassword" : string =  "";
  }
  
  export class CodeVM {
    "code" : string =  "";
  }
  export class EmailSmsVM {
    "email_sms" : string =  "";
  }
  
  export class EmailVM {
    "email" : string =  "";
  }
  
  export class SignUpVM {
    "code" : string =  "";
    "password" : string =  "";
    "tenantId" : string =  "";
    "username" : string =  "";
  }
  
  export class VerifyYourAccountCodeVM {
    "code" : string =  "";
    "factor" : string =  "";
    "sub" : string =  "";
    "session" : string =  "";
    "tenantId" : string =  "";
  }
  
  // export class LoginVM {
  //   "password" : string =  "";
  //   "tenantId" : string =  "";
  //   "username" : string =  "";
  //   "email":string="";
  // }
  export class LoginVM{
    "email":string="";
    "password" : string =  "";
    "isSignedIn":boolean=false;
  }
  export class VerificationVM {
    "code" : string =  "";
    "tenantId" : string =  "";
    "username" : string =  "";
  }