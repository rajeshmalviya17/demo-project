export enum AUTH_FLOW {
    LOGIN = "LOGIN",
    SIGNUP = "SIGNUP",
    RESET_PASSWORD = "RESET_PASSWORD",
  }
  
  export enum SCREEN {
    LOGIN = "LOGIN",
    VERIFY_YOUR_ACCOUNT = "VERIFY_YOUR_ACCOUNT",
    VERIFY_NEW_ACCOUNT = "VERIFY_NEW_ACCOUNT",
    VERIFY_YOUR_ACCOUNT_CODE = "VERIFY_YOUR_ACCOUNT_CODE",
    ENTER_EMAIL = "ENTER_EMAIL",
    CREATE_PASSWORD = "CREATE_PASSWORD",
    RESET_PASSWORD = "RESET_PASSWORD",
  }
  
  export enum MODUELE_NAME {
    AUTH = "auth",
  }
  
  export enum PAGE_TYPE {
    AUTH_PAGE = "authpage",
    MISSION_LIST_PAGE = "mission-list-page",
    SEARCH_BOX = "searchbox",
  }
  
  export enum API_URLS {
    GET_MY_SELF = "me",
    INITIATE_CONSOLE_LOGIN = "initiateConsoleLogin",
    INITIATE_RESET_PASSWORD = "initiateResetPassword",
    LOGIN = "login",
    RESET_PASSWORD = "resetPassword",
    CONSOLE_SIGNUP = "consoleSignup",
    REFRESH_ID_TOKEN = "refreshTokens",
  }
  
  export enum AUTH_CONST {
    LOCALHOST = "localhost",
    USERNAME = "username",
    CODE = "code",
    TAB_CLOSED = "tabClosed",
  }
  
  export enum AUTH_FORM_FIELDS {
    BASICS = "basic",
    EMAIL = "email",
    ERROR = "error",
    VERTICAL = "vertical",
    PASSWORD = "password",
    CONFIRM_PASSWORD = "confirmPassword",
  }
  
  
  export const tenantId = "jmp";
  