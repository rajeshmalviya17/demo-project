export interface InitiateLoginSession {
    session: string;
    sub: string;
    factor: string;
  }
  
  export interface Session {
    idToken: string;
    refreshToken: string;
  }
  
  export interface User {
    name: string;
    image: string;
  }
  