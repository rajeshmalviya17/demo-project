import React from "react";
import Form, {
  SimpleItem,
  EmailRule,
  RequiredRule,
  AsyncRule,
} from "devextreme-react/form";
import { useTranslation } from "react-i18next";
const passwordOptions = {
  mode: "password",
};
type LoginProps = {
  onSubmit: Function;
  error: boolean;
};
const dateBoxOptions = { width: "100%" };

const customer = {
  email: "",
  password: "",
};
const sendEmailRequest = (value: any) => {
  const validEmail = "rjs@gmail.com";
  return new Promise((resolve) => {
    setTimeout(function () {
      resolve(value === validEmail);
    }, 1000);
  });
};
const sendPasswordRequest = (value: any) => {
  const validPassword = "Rajesh17#";
  return new Promise((resolve) => {
    setTimeout(function () {
      resolve(value === validPassword);
    }, 1000);
  });
};
const asyncEmailValidation = (params: any) => {
  return sendEmailRequest(params.value);
};
const asyncPasswordValidation = (params: any) => {
  return sendPasswordRequest(params.value);
};
const Login: React.FC<LoginProps> = (props: LoginProps) => {
  const { t } = useTranslation();
  const { onSubmit } = props;
  const [isSubmitted, setIsSubmitted] = React.useState(false);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    setIsSubmitted(!isSubmitted);
    onSubmit(customer);
  };

  return (
    <>
      <div className="h-screen">
        <div className="container mx-auto h-full flex justify-center items-center">
          <div className="md:w-1/3 bg-black rounded-xl">
            <div className="m-8 py-8">
              <form action="your-action" onSubmit={handleSubmit}>
                <h1 className="text-white text-3xl mb-8 -mt-2 text-center">
                  Login
                </h1>
                <Form
                  formData={customer}
                  readOnly={false}
                  showColonAfterLabel={true}
                  validationGroup="customerData"
                  labelLocation="left"
                >
                  <SimpleItem
                    dataField={t("auth.label.email")}
                    editorType="dxTextBox"
                    editorOptions={dateBoxOptions}
                  >
                    <RequiredRule
                      message={t("auth.validation.emailRequired")}
                    />
                    <EmailRule message={t("auth.validation.incorrectEmail")} />
                    <AsyncRule
                      message={t("auth.validation.registeredEmail")}
                      validationCallback={asyncEmailValidation}
                    />
                  </SimpleItem>
                  <SimpleItem
                    dataField={t("auth.label.password")}
                    editorType="dxTextBox"
                    editorOptions={passwordOptions}
                  >
                    <RequiredRule
                      message={t("auth.validation.passwordRequired")}
                    />
                    <AsyncRule
                      message=""
                      validationCallback={asyncPasswordValidation}
                    />
                  </SimpleItem>
                </Form>
                <div className="px-4 pb-2 pt-8">
                  <button
                    type="submit"
                    className="uppercase inline-block  ml-20 md:w-2/3 p-4 text-sm  rounded-full bg-indigo-500 hover:bg-indigo-600 focus:outline-none"
                  >
                    {" "}
                    {t("adduser.label.submit")}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
