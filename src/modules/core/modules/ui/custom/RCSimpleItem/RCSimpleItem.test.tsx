import * as React from "react";
import { shallow } from "enzyme";
import RCSimpleItem from "./RCSimpleItem";

describe("RCSimpleItem", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<RCSimpleItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
