import { Item, SimpleItem } from "devextreme-react/form";
import * as React from "react";

type RCSimpleItemProps = {
  editorType: string;
  dataField: string;
  editorOptions: {};
};

const RCSimpleItem: React.FC<RCSimpleItemProps> = (
  props: RCSimpleItemProps
) => {
  const { dataField, editorType, editorOptions } = props;
  return (
    <Item
      dataField={dataField}
      editorType={editorType}
      editorOptions={editorOptions}
    ></Item>
  );
};

export default RCSimpleItem;
