import React from "react";

type RCCustomRuleProps = {
  RuleComponent?: React.ReactNode;
};

const RCCustomRule: React.FC<any> = (props:any) => {
  const {RuleComponent,message}=props;
  return (
   <>
   <RuleComponent message={message}/>
   </>
  )
};

export default RCCustomRule;
