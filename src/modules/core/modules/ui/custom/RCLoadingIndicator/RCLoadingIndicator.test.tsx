import * as React from "react";
import { shallow } from "enzyme";
import RCLoadingIndicator from "./RCLoadingIndicator";

describe("RCLoadingIndicator", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<RCLoadingIndicator />);
    expect(wrapper).toMatchSnapshot();
  });
});
