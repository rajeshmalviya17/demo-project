import { LoadIndicator } from "devextreme-react";
import * as React from "react";

type RCLoadingIndicatorProps = {
  //
};

const RCLoadingIndicator: React.FC<any> = () => {
  return (
    <div className="relative">
      <div className="w-1/2 absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2translate-x-1/2 translate-y-1/2">
        <LoadIndicator id="large-indicator" height={60} width={60} />
      </div>
    </div>
  );
};

export default RCLoadingIndicator;
