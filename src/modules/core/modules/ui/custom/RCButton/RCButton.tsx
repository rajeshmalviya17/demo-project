import * as React from "react";
import { Button } from 'devextreme-react/button';
type RCButtonProps = {
  width:string,
  text:string,
  type?: "default" | "back" | "danger" | "normal" | "success" | undefined,
  stylingMode?: "text" | "outlined" | "contained" | undefined,
  onClick?:  undefined
};

const RCButton: React.FC<RCButtonProps> = (props:RCButtonProps) => {
  const {width,text,type,stylingMode,onClick}=props;
  return (
    <Button
    width={width}
    text={text}
    type={type}
    stylingMode={stylingMode}
    onClick={onClick}
  />
  );
};

export default RCButton;
