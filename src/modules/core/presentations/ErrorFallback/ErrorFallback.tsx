import * as React from "react";
import { CORE_CONST } from "../../models/enums/core.enums";

type ErrorFallbackProps = {};

const ErrorFallback: React.FC<ErrorFallbackProps> = ( props: ErrorFallbackProps ) => {
  
  return (
    <div role = { CORE_CONST.ALERT } >
      <p>Something went wrong:</p>
      {/* <pre style={{ color: "red" }}>{error.message}</pre> */}
    </div>
  );
};

export default ErrorFallback;
