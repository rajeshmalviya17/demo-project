
  export enum APPLICATION_PLATFORM {
    MOBILE = "mobile",
    WEB = "web",
  }
  export enum DATA_AND_TIME {
    "MM/DD/YYYY" = "MM/DD/YYYY",
    "DD.MM.YY" = "DD.MM.YY",
    "DD.MM.YYYY" = "DD.MM.YYYY",
    "h:mmA" = "h:mmA",
    "MM/DD/YYYY ; h:mmA" = "MM/DD/YYYY ; h:mmA",
  }
  
  export enum SIDEBAR_OPERATIONS {
    CREATE = "CREATE",
    UPDATE = "UPDATE",
    DETAIL = "DETAIL",
    DUPLICATE = "DUPLICATE",
    CREATE_GROUPED_FEATURE = "CREATE_GROUPED_FEATURE",
    UPDATE_GROUPED_FEATURE = "UPDATE_GROUPED_FEATURE",
    DETAIL_GROUPED_FEATURE = "DETAIL_GROUPED_FEATURE",
  }
  
  export enum COLOR_NAME {
    GREEN = "green",
    YELLOW = "yellow",
    RED = "red",
  }
  
  export enum BUTTON_ACTION_TYPE {
    OK = "OK",
    SAVE_FEATURE_DISABLED = "SAVE_FEATURE_DISABLED",
    SAVE_FEATURE = "SAVE_FEATURE",
    ADD = "ADD",
    ACCEPT = "ACCEPT",
    REJECT = "REJECT",
    CREATE_GROUPED_FEATURE = "CREATE_GROUPED_FEATURE",
    PlayBack = "PlayBack",
    PlayForward = "PlayForward",
    PlayVideo = "PlayVideo",
  }
  
  export enum SUBMIT_BUTTON_ACTION_TYPE {
    DELETE = "DELETE",
    EDIT = "EDIT",
    CREATE = "CREATE",
    VERIFY_YOUR_ACCOUNT_CODE = "VERIFY_YOUR_ACCOUNT_CODE",
    AUTH = "AUTH",
    SAVE_USER_PROFILE = "SAVE_USER_PROFILE",
  }
  
  export enum ACTION_TYPES {
    EDIT = "EDIT",
    DELETE = "DELETE",
    DUPLICATE = "DUPLICATE",
  }
  
  export enum HTTP_STATUS_CODES {
    UNAUTHORIZED = 401,
  }
  
  export enum CALENDER_VALUES {
    WEEK = "week",
    START = "start",
    END = "end",
    TIME_GUTTER_FORMAT = "h A",
    DAY_FORMAT = "ddd D",
  }
  
  export enum API_RESPONCE {
    DELETED = "deleted",
    UPDATED = "updated",
  }
  
  export enum FILE_TYPES {
    KML = "kml",
    TEXT_XML = "text/xml",
  }
  
  export enum ACTION_EVENTS {
    MOVE = "move",
    LOAD = "load",
  }
  
  export enum MAP_CONST {
    CUSTOM_MARKER = "custom-marker",
    FEATURE_COLLECTION = "FeatureCollection",
    FILL = "fill",
    FILL_OPACITY = "fill-opacity",
    GEOJSON = "geojson",
    GET = "get",
    LAYER_DATA = "layerData",
    LINE = "line",
    LINE_STRING = "LineString",
    LINE_STRING_LAYER_DATA = "lineStringLayerData",
    MAP_LAYER_DATA = "mapLayerData",
    MARKER_LAYER_DATA = "markerLayerData",
    OUTLINE = "outline",
    POINT = "Point",
    POINTS = "points",
    POLYGON = "Polygon",
    POLYGON_LAYER_DATA = "polygonLayerData",
    ROUND = "round",
    ROUTE = "route",
    STROKE = "stroke",
    STROKE_OPACITY = "stroke-opacity",
    STROKE_WIDTH = "stroke-width",
    SYMBOL = "symbol",
  }
  
  export enum CORE_CONST {
    EMAIL = "email",
    ALERT = "alert",
    SMS = "sms",
  }
  
  export enum CORE_FORM_FIELDS {
    ALLOWED_GEOJSON_EXT = ".kml" /* ".kml, .geojson", */,
    AVATAR = "avatar",
    NUMBER = "number",
    BASICS = "basic",
    COLOR = "color",
    ERROR = "error",
    FILE_UPLOAD = "file-upload",
    LARGE = "large",
    LINK = "link",
    PRIMARY = "primary",
    SEARCH = "search",
    SUBMIT = "submit",
    TIME_PICKER_FORMAT = "h:mm A",
    VERTICAL = "vertical",
    WEEK = "week",
  }
  
  export enum MISSION_FILTER_MENU_TITLE {
    STATUS = "Status",
    INSPECTION = "Inspection",
    PILOT = "Pilot",
    UAV = "UAV",
    MISSION_NAME = "Mission Name",
  }
  
  export enum MISSION_FILTER_MENU_KEY {
    STATUS = "status",
    INSPECTION = "inspection_id",
    PILOT = "pilot_id",
    UAV = "uav_id",
    MISSION_NAME = "Mission Name",
  }
  
  export enum MISSION_SORT_MENU_TITLE {
    MISSION_NAME = "Mission Name",
    INSPECTION = "Inspection",
    PILOT = "Pilot",
    UAV = "UAV",
    DATE_TIME = "Date/Time",
    STATUS = "status",
  }
  
  export enum MISSION_SORT_MENU_KEY {
    MISSION_NAME = "Mission Name",
    MISSION_NAME_ASC = "missionNameAsc",
    MISSION_NAME_DEC = "missionNameAsc",
    INSPECTION = "inspection_name",
    INSPECTION_ASC = "inspectionAsc",
    INSPECTION_DEC = "inspectionDesc",
    PILOT = "Pilot",
    PILOT_ASC = "pilotAsc",
    PILOT_DEC = "pilotDesc",
    UAV = "UAV",
    UAV_ASC = "uavAsc",
    UAV_DEC = "uavDesc",
    DATE_TIME = "Date/Time",
    DATE_TIME_ASC = "dateAsc",
    DATE_TIME_DEC = "dateDesc",
    STATUS = "status",
    STATUS_ASC = "statusAsc",
    STATUS_DEC = "statusDesc",
  }
  
 
  