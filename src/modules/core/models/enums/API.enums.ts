export enum API_URLS {
    GET_MY_SELF = "me",
    INITIATE_CONSOLE_LOGIN = "initiateConsoleLogin",
    INITIATE_RESET_PASSWORD = "initiateResetPassword",
    LOGIN = "login",
    RESET_PASSWORD = "resetPassword",
    CONSOLE_SIGNUP = "consoleSignup",
    REFRESH_ID_TOKEN = "refreshTokens",
  }