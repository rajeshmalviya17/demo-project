import axios from "axios";
import { createContext, useContext } from "react";
import { authStore } from "./../../auth/states/auth";
import { EAComponentProps } from "../models/interfaces/component.interface";

export interface ApiOperations {
  auth: Function;
  get: Function;
  post: Function;
  put: Function;
  remove: Function;
  login: Function;
}
const APIServiceContext = createContext<ApiOperations | null>(null);

export const APIServiceProvider = (props: EAComponentProps) => {
  const auth = (url: string, body: any) => {
    return axios
      .post(url, body)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });
  };

  const get = (url: string) => {
    return axios
      .get(url)
      .then(function (response: any) {
        return response;
      })
      .catch(function (error) {
        return error;
      });
  };
  const login = (body: any) => {
    const { email, password } = body;
    if (email === "rjs@gmail.com" && password === "Rajesh17#") {
      authStore.update({ email, isSignedIn: true });
      return { status: 200 };
    } else {
      return { status: 400 };
    }
  };

  const post = (url: string, body: any) => {
    return axios
      .post(url, body)
      .then(function (response) {
        return Promise.resolve("Created");
      })
      .catch(function (error) {
        return error;
      });
  };

  const put = (url: string, body: any) => {
    return axios
      .put(url, body)
      .then(function (response) {
        return Promise.resolve("updated");
      })
      .catch(function (error) {
        return error;
      });
  };

  const remove = (url: string, body?: any) => {
    return axios
      .delete(url, body)
      .then(function (response) {
        return Promise.resolve("deleted");
      })
      .catch(function (error) {
        return error;
      });
  };

  const ApiOperations: ApiOperations = {
    auth,
    get,
    post,
    put,
    remove,
    login,
  };

  return (
    <APIServiceContext.Provider value={ApiOperations}>
      {props.children}
    </APIServiceContext.Provider>
  );
};

export const useAPIService = () => {
  return useContext(APIServiceContext);
};
