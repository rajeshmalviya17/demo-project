import { akitaDevtools, persistState, resetStores } from "@datorama/akita";
import React, { createContext, useContext } from "react";
import SimpleCrypto from "simple-crypto-js";
import { CoreConstants } from "../models/constants/core.constants";
import { EAComponentProps } from "../models/interfaces/component.interface";

export interface CoreServiceOperations {
  setUpApplication : Function
  tearDownApplication : Function
};
const CoreServiceContext = createContext< CoreServiceOperations | null >( null );

export const CoreServiceProvider = (props: EAComponentProps) => {
  let simpleCrypto: SimpleCrypto;
  let persistStorage: any;

  const setUpApplication = (body: any) => {
    setupCryptography();
    setupStateManagement();
  };

  const tearDownApplication = () => {
    resetStateManagement();
  };

  const setupCryptography = () => {
    simpleCrypto = new SimpleCrypto(CoreConstants.APP_SECRET_KEY);
  };

  const setupStateManagement = () => {
    if (process.env.NODE_ENV !== "production") {
      akitaDevtools();
    }

    persistStorage = persistState({
      include: ["auth"],
      preStorageUpdate(storeName, state) {
        if (simpleCrypto) {
          return simpleCrypto.encrypt(state);
        }
        return state;
      },
      preStoreUpdate(storeName, state) {
        if (simpleCrypto) {
          return simpleCrypto.decrypt(state);
        }
        return state;
      },
    });
  };

  const resetStateManagement = () => {
    resetStores();

    if (persistStorage) {
      setTimeout(() => {
        persistStorage.clearStore();
      }, 0);
    }
  };

  const CoreServiceOperations : CoreServiceOperations = {
    setUpApplication,
    tearDownApplication,
  };

  return (
    <CoreServiceContext.Provider value={CoreServiceOperations}>
      {props.children}
    </CoreServiceContext.Provider>
  );
};

export const useCoreService = () => {
  return useContext(CoreServiceContext);
};
