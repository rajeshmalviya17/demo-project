import * as React from "react";
import NavigationList from "../../presentations/NavigationList";

type NavigationListContainerProps = {
  //
};

const NavigationListContainer: React.FC<NavigationListContainerProps> = (props:NavigationListContainerProps) => {
  return <NavigationList/>
};

export default NavigationListContainer;
