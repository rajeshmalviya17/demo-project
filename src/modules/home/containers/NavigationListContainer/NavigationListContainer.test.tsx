import * as React from "react";
import { shallow } from "enzyme";
import NavigationListContainer from "./NavigationListContainer";

describe("NavigationListContainer", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<NavigationListContainer />);
    expect(wrapper).toMatchSnapshot();
  });
});
