import * as React from "react";
import { shallow } from "enzyme";
import HomeContainer from "./HomeContainer";

describe("HomeContainer", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<HomeContainer />);
    expect(wrapper).toMatchSnapshot();
  });
});
