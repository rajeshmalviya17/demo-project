import React, { useState } from "react";
import { useHistory } from "react-router";
import HomeRoutes from "./HomeRoutes";
import { Drawer, Toolbar } from "devextreme-react";
import NavigationListContainer from "./containers/NavigationListContainer";
import { authQuery } from "../auth/states/auth";
import {
  AuthFacadeOperations,
  useAuthFacade,
} from "../auth/facades/Auth.facade";

const Home = () => {
  const authFacade: AuthFacadeOperations | null = useAuthFacade();
  const history = useHistory();
  const [open, setOpen] = useState<boolean>(true);
  const logoutHandler = () => {
    authFacade?.logOut();
    history.push("/auth");
  };
  const userPanelText = authQuery.getValue().email;
  const toolbarItems = [
    {
      widget: "dxButton",
      location: "before",
      cssClass: "h-14",
      options: {
        icon: "menu",
        text: "Menu",
        onClick: () => setOpen(!open),
      },
    },
    {
      widget: "dxButton",
      location: "after",
      cssClass: "h-14 p-4",
      options: {
        icon: "user",
        text: userPanelText,
      },
    },
    {
      widget: "dxButton",
      location: "after",
      cssClass: "h-14 p-4",
      options: {
        icon: "key",
        text: "Logout",
        onClick: logoutHandler,
      },
    },
  ];

  return (
    <>
      <Toolbar items={toolbarItems} className="h-14" />
      <Drawer
        opened={open}
        openedStateMode={"shrink"}
        position={"left"}
        revealMode={"slide"}
        component={NavigationListContainer}
        closeOnOutsideClick={true}
        height={575}
      >
        <div id="view">
          <HomeRoutes />
        </div>
      </Drawer>
    </>
  );
};

export default Home;
