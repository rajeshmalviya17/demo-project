import { LoadIndicator } from "devextreme-react";
import React, { Suspense } from "react";
import { Route, Switch, useRouteMatch } from "react-router";

const UserListPage = React.lazy(() => import("./pages/UserListPage"));

const UserListRoutes = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Suspense
        fallback={<LoadIndicator id="large-indicator" height={60} width={60} />}
      >
        <Route path={match.path + "/"} exact={true}>
          <UserListPage />
        </Route>
        {/* <Route>
        <PageNotFound />
      </Route> */}
      </Suspense>
    </Switch>
  );
};

export default UserListRoutes;
