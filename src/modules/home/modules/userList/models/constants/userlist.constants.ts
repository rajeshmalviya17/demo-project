export const columns = ['name', 'email','age','qualification','birthDate'];
export const displayModes = [
    { text: "Display Mode 'full'", value: "full" },
    { text: "Display Mode 'compact'", value: "compact" },
  ];
  export const position = { of: "#employee" };