import React, { createContext, useContext } from "react";
import {
  UsersRepoOperations,
  useUsersRepository,
} from "../repository/UserList.repository";

export interface UsersServiceOperations {
  getUsersList: Function;
}

const UsersServiceContext = createContext<UsersServiceOperations | null>(null);
export const UsersServiceProvider = (props: any) => {
  const UsersRepository: UsersRepoOperations | null = useUsersRepository();
  const getUsersList = () => {
    return UsersRepository?.getUsersList();
  };
  const UsersServiceOperations: UsersServiceOperations = {
    getUsersList,
  };

  return (
    <UsersServiceContext.Provider value={UsersServiceOperations}>
      {props.children}
    </UsersServiceContext.Provider>
  );
};
export const useUsersService = () => {
  return useContext(UsersServiceContext);
};
