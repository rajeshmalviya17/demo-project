import * as React from "react";
import { shallow } from "enzyme";
import UserListPage from "./UserListPage";

describe("UserListPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<UserListPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
