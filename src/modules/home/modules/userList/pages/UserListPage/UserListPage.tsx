import * as React from "react";
import UserListContainer from "../../containers/UserListContainer";

type UserListPageProps = {
  //
};

const UserListPage: React.FC<UserListPageProps> = (props:UserListPageProps) => {
  return <UserListContainer/>;
};

export default UserListPage;
