import * as React from "react";
import {
  UsersFacadeOperations,
  useUsersFacade,
} from "../../facades/UserList.facade";
import UserList from "../../presentations/UserList";

type UserListContainerProps = {
  //
};
type UserProps = { id: string; name: any; email: any };
const UserListContainer: React.FC<UserListContainerProps> = (props:UserListContainerProps) => {
  const [users, setUsers] = React.useState<UserProps[] | []>([]);
  const usersFacade: UsersFacadeOperations | null = useUsersFacade();
  const [loading, setLoading] = React.useState<boolean | undefined>(true);

  const GetUsersList = async (params?: any) => {
    usersFacade?.getUsersList(params).then((response: any) => {
      const loadedUsers = [];
      for (const key in response) {
        loadedUsers.push({
          id: key,
          name: response[key].name,
          email: response[key].email,
          age: response[key].Age,
          qualification: response[key].Qualifications,
          birthDate: response[key].BirthDate.substring(0, 10),
        });
      }
      setUsers(loadedUsers);
      setLoading(false);
    });
  };
  React.useEffect(() => {
    GetUsersList();
  }, []);
  return <UserList users={users} loading={loading} />;
};

export default UserListContainer;
