import React, { useState } from "react";
import DataGrid, {
  Editing,
  FilterPanel,
  FilterRow,
  HeaderFilter,
  Pager,
  Paging,
  Sorting,
} from "devextreme-react/data-grid";
import { columns, position } from "../../models/constants/userlist.constants";
import { LoadPanel } from "devextreme-react";

type UserListProps = {
  //
};
const UserList: React.FC<any> = (props) => {
  const allowedPageSizes = [5, 10, "all"];
  const [ModeChange, setModeChange] = useState<string | null>();
  const [PageSizeSelectorChange, setPageSizeSelectorChange] = useState<
    string | null
  >();
  const [InfoChange, setInfoChange] = useState<string | null>();
  const [NavButtonsChange, setNavButtonsChange] = useState<string | null>();
  return (
    <div id="employee" className="flex p-8">
      <DataGrid
        dataSource={props.users}
        keyExpr="id"
        defaultColumns={columns}
        showBorders={true}
      >
        <Paging defaultPageSize={5} />
        <Sorting mode="multiple" />
        <HeaderFilter visible={true} />
        <FilterPanel visible={true} />
        <FilterRow visible={true} />
        <Pager
          visible={true}
          allowedPageSizes={allowedPageSizes}
          displayMode={ModeChange}
          showPageSizeSelector={PageSizeSelectorChange}
          showInfo={InfoChange}
          showNavigationButtons={NavButtonsChange}
        />

        <LoadPanel
          shadingColor="rgba(0,0,0,0.4)"
          position={position}
          visible={props.loading}
          showIndicator={true}
          shading={true}
          showPane={true}
          closeOnOutsideClick={true}
        />
        <Editing
          mode="form"
          allowUpdating={true}
          allowAdding={true}
          allowDeleting={true}
        />
      </DataGrid>
    </div>
  );
};

export default UserList;
