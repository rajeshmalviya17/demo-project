import { createContext, useContext } from "react";
import { EAComponentProps } from "../../../../core/models/interfaces/component.interface";
import {
  ApiOperations,
  useAPIService,
} from "../../../../core/services/API.service";

const {
  REACT_APP_API_URL
} = process.env;

export interface UsersRepoOperations {
  getUsersList: Function;
}

const UsersRepositoryContext = createContext<UsersRepoOperations | null>(null);
export const UsersRepositoryProvider = (props: EAComponentProps) => {
  const APIService: ApiOperations | null = useAPIService();

  const getUsersList = () => {
    return APIService?.get(`${REACT_APP_API_URL}`).then(function (
      response: any
    ) {
      if (response && response.data) {
        return response.data;
      }
    });
  };

  const UsersRepoOperations: UsersRepoOperations = {
    getUsersList,
  };

  return (
    <UsersRepositoryContext.Provider value={UsersRepoOperations}>
      {props.children}
    </UsersRepositoryContext.Provider>
  );
};

export const useUsersRepository = () => {
  return useContext(UsersRepositoryContext);
};
