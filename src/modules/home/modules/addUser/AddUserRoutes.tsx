import { LoadIndicator } from "devextreme-react";
import React, { Suspense } from "react";
import { Route, Switch, useRouteMatch } from "react-router";
const AddUserPage = React.lazy(() => import('./pages/AddUserPage'));

const AddUserRoutes = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Suspense fallback={<LoadIndicator id="large-indicator" height={60} width={60} />}>
        <Route path={match.path + "/"} exact={true}>
          <AddUserPage />
        </Route>
        {/* <Route>
        <PageNotFound />
      </Route> */}
      </Suspense>
    </Switch>
  );
};

export default AddUserRoutes;
