import React, { createContext, useContext } from "react";
import { EAComponentProps } from "../../../../core/models/interfaces/component.interface";
import {
  ApiOperations,
  useAPIService,
} from "../../../../core/services/API.service";

const { REACT_APP_API_URL } = process.env;

export interface UserformRepoOperations {
  createUser: Function;
}
const UserformRepositoryContext = createContext<UserformRepoOperations | null>(null);

export const UserformRepositoryProvider = (props: EAComponentProps) => {
  const APIService: ApiOperations | null = useAPIService();
  const createUser = (body: any) => {
    return APIService?.post(REACT_APP_API_URL, body);
  };

  const UserformRepoOperations: UserformRepoOperations = {
    createUser,
  };

  return (
    <UserformRepositoryContext.Provider value={UserformRepoOperations}>
      {props.children}
    </UserformRepositoryContext.Provider>
  );
};

export const useUserformRepository = () => {
  return useContext(UserformRepositoryContext);
};
