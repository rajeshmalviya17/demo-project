import * as React from "react";
import { shallow } from "enzyme";
import AddUserContainer from "./AddUserContainer";

describe("AddUserContainer", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<AddUserContainer />);
    expect(wrapper).toMatchSnapshot();
  });
});
