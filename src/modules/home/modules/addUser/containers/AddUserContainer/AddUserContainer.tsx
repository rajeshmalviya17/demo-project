import * as React from "react";
import AddUser from "../../presentations/AddUser";
import {
  UserformFacadeOperations,
  useUserformFacade,
} from "../../facades/AddUser.facade";

type AddUserContainerProps = {
  //
};

const AddUserContainer: React.FC<AddUserContainerProps> = (props:AddUserContainerProps) => {
  const userformFacade: UserformFacadeOperations | null = useUserformFacade();
  const handleOnCreateUser = (formData: any) => {
    // notify("Data Added Successfully");
    userformFacade?.createUser(formData).then((response: any) => {
     
    return response;
    });
  };
  return <AddUser onCreateUser={handleOnCreateUser}/>;
};

export default AddUserContainer;
