import * as React from "react";
import AddUserContainer from "../../containers/AddUserContainer";

type AddUserPageProps = {
  //
};

const AddUserPage: React.FC<AddUserPageProps> = (props:AddUserPageProps) => {
  return <AddUserContainer/>;
};

export default AddUserPage;
