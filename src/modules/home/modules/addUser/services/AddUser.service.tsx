import React, { createContext, useContext } from "react";
import { UserformRepoOperations, useUserformRepository } from "../repository/AddUser.repository";
export interface UserformServiceOperations {
    createUser : Function;
  };

  const UserformServiceContext = createContext< UserformServiceOperations | null >( null );
  export const UserformServiceProvider = (props: any) => {    
    const UserformRepository: UserformRepoOperations | null = useUserformRepository();
    const createUser = (body: any) => {      
        return UserformRepository?.createUser(body);
      };

      const UserformServiceOperations : UserformServiceOperations = {
        createUser
      };

      return (
        <UserformServiceContext.Provider value={UserformServiceOperations}>
          {props.children}
        </UserformServiceContext.Provider>
      );
  }
  export const useUserformService = () => {
    return useContext(UserformServiceContext);
  };
  