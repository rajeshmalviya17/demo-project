import * as React from "react";
import "devextreme/dist/css/dx.light.css";
import {
  Form,
  SimpleItem,
  GroupItem,
  TabbedItem,
  Tab,
  TabPanelOptions,
  RequiredRule,
  EmailRule,
} from "devextreme-react/form";
import { useTranslation } from "react-i18next";
import {
  employee,
  birthDateOptions,
  qualificationOptions,
} from "../../models/constants/adduser.constants";

type AddUserProps = {
  onCreateUser: Function;
};

const AddUser: React.FC<AddUserProps> = (props: AddUserProps) => {
  const { onCreateUser } = props;
  const { t } = useTranslation();
  const handleSubmit = (e: any) => {
    e.preventDefault();
    onCreateUser(employee);
  };

  return (
    <div className="h-full">
      <div className="container mx-auto h-full flex justify-center items-center mt-2">
        <div className="md:w-1/2 bg-black rounded-xl">
          <div className="m-4">
            <form action="" onSubmit={handleSubmit}>
              <Form formData={employee} colCount={1} showColonAfterLabel={true}>
                <GroupItem caption={t("adduser.label.personalInformation")}>
                  <TabbedItem cssClass="md:w-full p-4 clear-both">
                    <TabPanelOptions height="350" />
                    <Tab title={t("adduser.label.titleInfo")}>
                      <SimpleItem dataField={t("adduser.label.name")}>
                        <RequiredRule
                          message={t("auth.validation.nameRequired")}
                        />
                      </SimpleItem>
                      <SimpleItem dataField={t("adduser.label.email")}>
                        <RequiredRule
                          message={t("auth.validation.emailRequired")}
                        />
                        <EmailRule
                          message={t("auth.validation.incorrectEmail")}
                        />
                      </SimpleItem>
                      <SimpleItem
                        dataField="BirthDate"
                        editorType="dxDateBox"
                        editorOptions={birthDateOptions}
                      >
                        <RequiredRule
                          message={t("auth.validation.birthdateRequired")}
                        />
                      </SimpleItem>
                      <SimpleItem
                        dataField="Qualifications"
                        editorType="dxSelectBox"
                        editorOptions={qualificationOptions}
                      >
                        <RequiredRule
                          message={t("auth.validation.qualificationRequired")}
                        />
                      </SimpleItem>
                      <SimpleItem dataField="Age" editorType="dxNumberBox">
                        <RequiredRule
                          message={t("auth.validation.ageRequired")}
                        />
                      </SimpleItem>
                      <SimpleItem dataField="T&C" editorType="dxCheckBox">
                        <RequiredRule
                          message={t("auth.validation.tncRequired")}
                        />
                      </SimpleItem>
                    </Tab>
                  </TabbedItem>
                </GroupItem>
              </Form>

              <button
                type="submit"
                className="uppercase inline-block md:ml-24  md:w-2/3 p-4 text-sm  rounded-full bg-indigo-500 hover:bg-indigo-600 focus:outline-none"
              >
                {" "}
                {t("adduser.label.submit")}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddUser;
