import { List } from "devextreme-react";
import { Link } from "react-router-dom";
import { navigation } from "../../models/constants/home.constants";
type NavigationListProps = {
  //
};

const NavigationList: React.FC<NavigationListProps> = (
  props: NavigationListProps
) => {
  const renderItem = (navigation: any) => {
    return (
      <div>
        <Link to={"/" + navigation.path}>
          <div>
            <div className="dx-list-item-icon-container">
              <i
                className={`dx-icon dx-list-item-icon dx-icon-${navigation.icon}`}
              ></i>
            </div>
            <span className="text-white">{navigation.text}</span>
          </div>
        </Link>
      </div>
    );
  };
  return (
    <div className="list" style={{ width: "200px", height: "800px" }}>
      <List
        dataSource={navigation}
        hoverStateEnabled={false}
        selectionMode={"single"}
        activeStateEnabled={false}
        focusStateEnabled={false}
        itemRender={renderItem}
        className="bg-black opacity-60"
      />
    </div>
  );
};

export default NavigationList;
