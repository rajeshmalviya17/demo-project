import * as React from "react";
import { shallow } from "enzyme";
import NavigationList from "./NavigationList";

describe("NavigationList", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<NavigationList />);
    expect(wrapper).toMatchSnapshot();
  });
});
