import React from "react";
import { Suspense } from "react";
import { useTranslation } from "react-i18next";
import { Redirect, Route, Switch, useRouteMatch } from "react-router";
const AddUser = React.lazy(() => import("./modules/addUser/AddUser"));
const UserList = React.lazy(() => import("./modules/userList/UserList"));

const HomeRoutes = () => {
  const { t } = useTranslation();
  const match = useRouteMatch();
  return (
    <Switch>
      <Suspense fallback={<div>{t("app.label.loading")}</div>}>
        <Route path={match.path + "/userlist"}>
          <UserList />
        </Route>

        <Route path={match.path + "/adduser"}>
          <AddUser />
        </Route>

        <Redirect
          path={match.path + "/"} //localhost:3000/home/adduser
          exact={true}
          to={match.path + "/adduser"}
        ></Redirect>
        {/* <Route>
          <PageNotFound />
        </Route> */}
      </Suspense>
    </Switch>
  );
};

export default HomeRoutes;
