import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./i18n";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "devextreme/dist/css/dx.generic.custom-scheme.css";
import { BrowserRouter } from "react-router-dom";
import { APIServiceProvider } from "./modules/core/services/API.service";

import { CoreServiceProvider } from "./modules/core/services/Core.service";
import { CoreFacadeProvider } from "./modules/core/facades/Core.facades";
import { AuthRepositoryProvider } from "./modules/auth/repository/Auth.repository";
import { AuthServiceProvider } from "./modules/auth/services/Auth.service";
import { AuthFacadeProvider } from "./modules/auth/facades/Auth.facade";
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <APIServiceProvider>
        <AuthRepositoryProvider>
          <AuthServiceProvider>
            <AuthFacadeProvider>
              <CoreServiceProvider>
                <CoreFacadeProvider>
                  <App />
                </CoreFacadeProvider>
              </CoreServiceProvider>
            </AuthFacadeProvider>
          </AuthServiceProvider>
        </AuthRepositoryProvider>
      </APIServiceProvider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
